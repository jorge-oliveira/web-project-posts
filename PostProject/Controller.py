"""
 This project is a social network for devs
 Is build with bootstrap and bootstrap material
"""

import web
from PostProject.Models import RegisterModel
from PostProject.Models import LoginModel
from PostProject.Models import Posts
import os

# disable the debug mode, interferes with the sessions
web.config.debug = False

urls = (
    '/', 'Home',
    '/register', 'Register',
    '/login', 'Login',
    '/logout', 'Logout',
    '/postregistration', 'PostRegistration',
    '/check-login', 'CheckLogin',
    '/post-activity', 'PostActivity',
    '/profile/(.*)/info', 'UserInfo',
    '/settings', 'UserSettings',
    '/update-settings', 'UpdateSettings',
    '/profile/(.*)', 'UserProfile',
    '/submit-comment', 'SubmitComment',
    '/upload-image/(.*)', 'UploadImage',
    '/testlink', 'TestLink',
    '/testlinkuser/(.*)', 'TestLinkUser',
    '/testlink/another-link', 'AnotherLink',
    '/testlinkdobleroutes', 'TestLinkOtherRoute'
)


# set the location of the html file with a base page
app = web.application(urls, globals())

# create a global variable session and define the user keyword for control the session
session = web.session.Session(app, web.session.DiskStore("sessions"), initializer={"user": None})

# initialize the session
session_data = session._initializer

# refactor the render for implement globals for sharing with all pages
render = web.template.render('Views/Templates/', base='MainLayout',
                             globals={'session': session_data, 'current_user': session_data['user']})

# Classes/Routes

class Home:
    def GET(self):
        #
        # # set manually the login for automatically do the login
        # data = type('obj', (object,), {"username": "jorge", "password": "12345"})
        #
        # login = LoginModel.LoginModel()
        # isCorrect = login.check_user(data)
        #
        # if isCorrect:
        #     session_data["user"] = isCorrect

        # instantiate post_model
        post_model = Posts.Posts()
        posts = post_model.get_all_posts()

        # send the response to Home file with all the posts
        return render.Home(posts)


class Register:
    def GET(self):
        return render.Register()


class Login:
    def GET(self):
        return render.Login()


class PostRegistration:
    def POST(self):
        data = web.input()
        print("The data received: ", data)

        reg_model = RegisterModel.RegisterModel()
        reg_model.insert_user(data)

        return data.username


class CheckLogin:
    def POST(self):
        # capture the data request
        data = web.input()
        print("The data CheckLogin received: ", data)

        # instantiate a LoginModel
        login = LoginModel.LoginModel()
        # check if the user exist
        isCorrect = login.check_user(data)

        if isCorrect:
            # create the session data
            session_data["user"] = isCorrect
            return isCorrect
        else:
            return "error"


class PostActivity:
    def POST(self):
        # catch the data
        data = web.input()
        # create the session data
        data.username = session_data["user"]["username"]
        # instantiate post model
        post_model = Posts.Posts()
        # call the method for insert the post data
        return post_model.insert_post(data)


class Logout:
    def GET(self):

        # removes user from the session before kill the session
        session["user"] = None
        session_data["user"] = None

        session.kill()
        return "success"


class UserProfile:
    def GET(self, user):
        # instantiate the LoginModel
        login = LoginModel.LoginModel()
        # get the user info based on the user login
        user_info = login.get_profile(user)

        # instantiate post_model
        post_model = Posts.Posts()
        posts = post_model.get_user_posts(user)

        # send the response to Profile file with all the posts
        return render.Profile(posts, user_info)


class UserInfo:
    def GET(self, user):
        # instantiate the LoginModel
        login = LoginModel.LoginModel()
        # get the user info based on the user login
        user_info = login.get_profile(user)
        # send the response of the user info to the page info
        return render.Info(user_info)


class UserSettings:
    def GET(self):

        # send the response
        return render.Settings()


class UpdateSettings:
    def POST(self):
        data = web.input()

        print("UpdateSettings data:", data)
        # setting the username from the session data
        data.username = session_data["user"]["username"]

        # instantiate loginModel
        settings_model = LoginModel.LoginModel()
        # call method for update the new data info
        if settings_model.update_info(data):
            return "success"
        else:
            return "A fatal error has occurred."


class SubmitComment:
    def POST(self):
        data = web.input()
        print("SubmitComment data: ", data)
        # setting the username equal the session_data
        data.username = session_data["user"]["username"]

        # instantiate post_model
        post_model = Posts.Posts()

        # insert the new comment data
        added_comment = post_model.add_comment(data)

        if added_comment:
            return added_comment
        else:
            # return a custom error ex: 403 in a json format
            return {"error": "403"}


class UploadImage:
    def POST(self, type):
        file = web.input(avatar={}, background={})

        # define the path to save
        file_dir = os.getcwd() + "/static/uploads/" + session_data["user"]["username"]
        print(file_dir)

        # if not exists, create the directory
        if not os.path.exists(file_dir):
            os.mkdir(file_dir)

        if "avatar" or "background" in file:
            filepath = file[type].filename.replace('\\', '/')
            filename = filepath.split("/")[-1]

            # open a file in binary mode
            f = open(file_dir + "/" + filename, "wb")

            f.write(file[type].file.read())
            f.close()

            update = {}
            update["type"] = type
            update["img"] = '/static/uploads/' + session_data["user"]["username"] + "/" + filename
            update["username"] = session_data["user"]["username"]

            account_model = LoginModel.LoginModel()
            update_avatar = account_model.update_image(update)

        # after the file upload, redirect to settings page
        raise web.seeother("/settings")

if __name__ == "__main__":
    app.run()
