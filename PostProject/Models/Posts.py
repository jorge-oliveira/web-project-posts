from pymongo import MongoClient
import datetime
import humanize
from bson import ObjectId

class Posts:
    def __init__(self):
        self.client = MongoClient()
        self.db = self.client.posts_db
        self.Users = self.db.users
        self.Posts = self.db.posts
        # create a collection for the comments
        self.Comments = self.db.comments

    def insert_post(self, data):
        # send post data to database
        inserted = self.Posts.insert({"username": data.username, "content": data.content, "date_added": datetime.datetime.now()})

        # capture the id of the inserted post
        post = self.Posts.find_one({"_id": inserted})

        new_post = {}
        new_post["name"] = self.Users.find_one({"username": post["username"]})["name"]
        new_post["content"] = post["content"]
        return post

    def get_all_posts(self):
        all_posts = self.Posts.find().sort("date_added", -1)
        # empty list
        new_posts = []

        # get all the posts from the user
        for post in all_posts:
            # get the content
            post["user"] = self.Users.find_one({"username": post["username"]})
            post["timestamp"] = humanize.naturaltime(datetime.datetime.now() - post["date_added"])

            # find the old comments by id, and convert id to a string
            post["old_comments"] = self.Comments.find({"post_id": str(post["_id"])})
            # create a empty list
            post["comments"] = []

            # Get all the coments from the user by id
            for comment in post["old_comments"]:
                print(comment)
                # set a new object
                comment["user"] = self.Users.find_one({"username": comment["username"]})
                comment["timestamp"] = humanize.naturaltime(datetime.datetime.now() - comment["date_added"])

                # Append all the objects
                post["comments"].append(comment)

            new_posts.append(post)

        return new_posts

    def get_user_posts(self, user):
        # print("inside: get_user_posts")
        # all_posts = self.Posts.find({"username": user}).sort({"date_added": -1})
        all_posts = self.Posts.find({"username": user})
        new_posts = []

        for post in all_posts:
            print(post)
            post["user"] = self.Users.find_one({"username": post["username"]})
            new_posts.append(post)

            return new_posts

    def add_comment(self, comment):
        print("The comments: ", comment)
        inserted = self.Comments.insert({"post_id": comment.post_id, "content": comment["comment-text"],
                                         "date_added": datetime.datetime.now(), "username": comment["username"]})
        # return all the inserted data
        return inserted