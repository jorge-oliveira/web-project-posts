from pymongo import MongoClient
import bcrypt


class LoginModel:
    def __init__(self):
        self.client = MongoClient()
        self.db = self.client.posts_db
        self.Users = self.db.users

    def check_user(self, data):
        # print("inside check_user method for checking if user exists.")
        user = self.Users.find_one({"username": data.username})

        # check the password
        if user:
            if bcrypt.checkpw(data.password.encode(), user["password"]):
                return user
            else:
                return False
        else:
            return False


    def update_info(self, data):

        print("The user to update:", data["username"])

        # Update the data on table users from database
        updated = self.Users.update_one({
            "username": data["username"]},
            {"$set": data})

        print("Data updated: ", updated)

        return True


    def get_profile(self, user):
        # find the userdata base on the username passed
        user_info = self.Users.find_one({"username": user})

        # return the info getted from database
        return user_info


    def update_image(self, update):
        updated = self.Users.update_one({"username": update["username"]},
                                        {"$set": {update["type"]: update["img"]}})

        return updated

