# import pymongo
from pymongo import MongoClient
import bcrypt

class RegisterModel:

    def __init__(self):
        self.client = MongoClient()
        self.db = self.client.posts_db
        self.Users = self.db.users

    def insert_user(self, data):
        print("data is: ", data)

        # encript the password
        hashed_pass = bcrypt.hashpw(data.password.encode(), bcrypt.gensalt())

        id = self.Users.insert({"username": data.username,
                                "name": data.display_name,
                                "password": hashed_pass,
                                "email": data.email,
                                "avatar": "",
                                "background": "",
                                "about": "",
                                "hobbies": "",
                                "birthday": ""})
        print("uid is", id)

        # find the user from database
        my_user = self.Users.find_once({"username": data.username})

        if bcrypt.checkpw("avocado1".encode(), my_user["password"]):
            print("this matches")
