$(document).ready(function() {
    // initialization of script material
    $('body').bootstrapMaterialDesign();
        console.log("loaded the material...");

    // sending an ajax request
    $(document).on("submit","#register-form", function (e) {

        // prevent default submitting type
        e.preventDefault();

        // capture the form data
        var form = $("#register-form").serialize(); // if get the error 500 it's the missing name attribute on the forms.
        console.log(form);

        // send the form over a ajax request
        $.ajax({
            url: '/postregistration',
            type: 'POST',
            data: form,
            success: function (response) {
                console.log(response);
            }
        });
    });

    $(document).on("submit","#login-form", function (e) {

        // prevent default submitting type
        e.preventDefault();

        // capture the form data
        var form = $("#login-form").serialize(); // if get the error 500 it's the missing name attribute on the forms.
        console.log(form);

        // send the form over a ajax request
        $.ajax({
            url: '/check-login',
            type: 'POST',
            data: form,
            success: function (res) {
                if (res == 'error'){
                    alert("could not log in.");
                }else{
                    console.log("Logged in as:", res);
                    // after login success redirect to other page in this case home
                    window.location.href = "/";
                }
            }
        });
    });

    $(document).on('click', '#logout-link', function (e) {
        e.preventDefault();

        $.ajax({
            url: '/logout',
            type: 'GET',
            success: function (res) {
                // in success logout redirect to login page
                if (res == 'success'){
                    window.location.href = '/login';
                }else{
                    alert("Something went wrong.");
                }

            }
        })

    });
    
    $(document).on("submit", "#post-activity", function (e) {
        e.preventDefault();

        // catch the form data
        form = $(this).serialize();

        $.ajax({
            url: "/post-activity",
            type: "POST",
            data: form,
            success: function (res) {
                // print the response that come from server
                console.log(res);
            }
        })
    });


    $(document).on('submit', '#settings-form', function(e){
        e.preventDefault();

        var form = $(this).serialize();
        $.ajax({
            url: '/update-settings',
            type: 'POST',
            data: form,
            success: function(res){
                if(res == "success"){
                    window.location.href = window.location.href;
                }else{
                    alert(res);
                }
            }
        });
    });

    $(document).on("submit", ".comment-form", function (e) {
        e.preventDefault();

        console.log("inside...");
        var form = $(this).serialize();

        $.ajax({
            url: '/submit-comment',
            type: 'POST',
            data: form,
            dataType: 'json',
            success: function (res) {
                console.log(res);
            }
        })

    })

});

