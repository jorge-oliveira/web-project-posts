import web

# setting routes
urls = (
    '/(.*)', 'index'
)

app = web.application(urls, globals())

class index:
    def GET(self, name):

        print("Hello ", name, '. How are you today?')
        # sending the return response to browser
        return "<h1>Hello " + name + '.</h1> How are you today?'

if __name__ == "__main__":
    app.run()
